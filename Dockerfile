FROM jenkins/jenkins:latest
USER root
RUN apt-get update && apt-get -y install sudo git nodejs npm docker.io
RUN git clone https://gitlab.com/mani639/mani639.git
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false
ENV CASC_JENKINS_CONFIG /var/jenkins_home/casc.yaml
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
COPY casc.yaml /var/jenkins_home/casc.yaml
EXPOSE 3000
