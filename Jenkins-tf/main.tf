provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {
    encrypt = true
    bucket = "mani639-terraform-state-storage-s3"
    region = "us-east-2"
    key = "terraform-state/terraform.tfstate"
  }
}

module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.3.0"

  name = "${var.jenkins_name}"

  ami                    = "${var.ami}"
  instance_type          = "${var.instance_type}"
  key_name               = "${var.key_name}"
  monitoring             = "${var.monitoring_status}"
  vpc_security_group_ids = "${var.vpc_security_group_ids}"
  subnet_id              = "${var.subnet_id}"
  iam_instance_profile   = "${var.iam_instance_profile}"
  #user_data              = "${file("install_jenkins.sh")}"
  user_data = <<EOF
#!/bin/bash
sudo yum update
sudo yum install git -y
sudo yum install docker -y
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker $USER
sudo chmod 666 /var/run/docker.sock
mkdir /home/ec2-user/mani
cd /home/ec2-user/mani
git clone https://gitlab.com/mani639/mani639.git
cd /home/ec2-user/mani/mani639
docker build -t mani639jenkins .
docker run --name mani639jenkins -p 8080:8080 -p 3000:3000 mani639jenkins
EOF

  tags = {
    Terraform   = "true"
    Environment = "${var.Environment}"
  }
}

module "web_server_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"

  name        = "${var.sg_name}"
  description = "Security group for jenkins server"
  vpc_id      = "${var.vpc_id}"

  ingress_cidr_blocks = ["10.10.0.0/16"]
}

resource "tls_private_key" "mani639" {
  algorithm = "RSA"
}

module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"

  key_name   = "${var.new_key_name}"
  public_key = tls_private_key.mani639.public_key_openssh
}
