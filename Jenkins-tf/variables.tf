variable "region" {
   default = "us-east-2"
}
variable "jenkins_name"{
   default = "mani639-jenkins"
}

variable "ami"{
   default = "ami-0b614a5d911900a9b"
}

variable "instance_type"{
   default = "t2.micro"
}

variable "key_name"{
   default = "mani639"
}

variable "monitoring_status"{
   default = false
}

variable "vpc_security_group_ids"{
   type    = list(string)
   default = ["sg-004a37799a24b11e3"]
}

variable "subnet_id"{
   default = "subnet-07b4621fab6b2b1f8"
}

variable "iam_instance_profile"{
   default = "ec2role"
}

variable "Environment"{
   default = "mani639jenkins"
}

variable "sg_name"{
   default = "mani639jenkins-server-sg"
}

variable "vpc_id"{
   default = "vpc-0740c1c3469710b79"
}

variable "new_key_name"{
   default = "mani6391"
}

variable "bucket_name"{
   default = "mani639-terraform-state-storage-s3"
}

variable "dynamodb_name"{
   default = "iac-terraform-state-lock-dynamo"
}
