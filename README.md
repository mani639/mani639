
This project uses terraform to build an EC2 Jenkins, pulls Jenkins config from this gitlab repo and also supports running a Node.JS applications coverage tests and report back this gitlab repo the build status.


 
I have created read only access to AWS account where my Jenkins EC2 sits. please use public IP of the EC2 yo access Jenkins UI.

How to use this built project:

Go to gitlab and make a commit mani-639 repo and go to Jenkins and verify the build 
        you will see a node.js application report in the build status 

Building new project:

Pre-Requisite: create Access token in GITLAB 
        click on user icon in top left -> select preferences -> Access tokens 
                provide token name, expiration and select all scopes and create( copy and save the token)

Copy the code from Jenkins.tf file in the repo and deploy/ terraform apply from your local then you will see instance deployed in AWS console. (modify variable according to your AWS account)
Then use this public IP to login to Jenkins using 8080 port (publicIP:8080). you will see a page asking to create User, create user and you now successfully logged into Jenkins.

Now you have to configure Jenkins:
        1. Under Manage Jenkins -> configure system -> scroll down to GITLAB and provide info below
            Name: GIT lab
            Gitlab HOST URL: https://gitlab.com/
            credentials: 
                    click on add -> select Jenkins from dropdown box and provide info:
                        Kind: Gitlab API token
                        API Token: provide API token created in GITLAB
                        ID : GITLab API toeken 

        2. Storing GITLAB account password in JENKINS credentials :
         under credential store -> click on Jenkins -> Global Credentials ->  Add credentials ->select secret text from dropdown  box add GIT Lab username and password
        3. Then go to Dash board create new Item :
                Item Name: provide project name
                select pipeline and save and use below steps to configure
                    Under build triggers: select build when change is pushed to GITLAB. 
                    copy pipeline script from Jenkins file in repo and paste in pipeline script.

Steps to do in GIT LAB:
  Open repo(mani-639) -> Integrations -> select Jenkins and configure
        provide Jenkins URL (https://publicIP:8080/).
        project name: "Jenkins project name"
        username:"Jenkins USER name"
        password: "Jenkins password"

Now you have setup everything and follow steps under How to use this built project to see output.
